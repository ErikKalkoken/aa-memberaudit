def get_input(text):
    """Wrapped input to enable unit testing / patching."""
    return input(text)
