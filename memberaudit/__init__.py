"""An Alliance Auth app that provides full access to Eve characters
and related reports for auditing, vetting and monitoring.
"""

# pylint: disable=invalid-name
default_app_config = "memberaudit.apps.MemberAuditConfig"

__version__ = "3.14.0"
__title__ = "Member Audit"
