from enum import IntEnum


class EvePlanetIds(IntEnum):
    AMAMAKE_I = 40161463
    AMAMAKE_II = 40161464
    AMAMAKE_III = 40161467
    AMAMAKE_IV = 40161469


class EveSolarSystemIds(IntEnum):
    AMAMAKE = 30002537
